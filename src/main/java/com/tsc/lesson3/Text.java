package com.tsc.lesson3;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * Класс, предназначенный для загрузки и хранения текста из файла,
 * а так же для получения информации о количестве букв в этом тексте, согласно заданию.
 *
 * @author Дмитрий Лаврухин
 */
public class Text {

    private final String fileName;
    private final ConcurrentHashMap<Character, Integer> storage;
    private final String text;

    public Text(String fileName) throws IOException {
        this.fileName = fileName;
        this.storage = new ConcurrentHashMap<>();
        this.text = getFileString().toUpperCase();
    }

    /**
     * Метод возвращает список букв, с указанием их количества.
     *
     * @return объект HashMap, в котором ключ это буква, а значение это количество вхождений этой буквы в текст
     */
    public Map<Character, Integer> getCountOfEachLetter() {
        CountDownLatch countDownLatch = new CountDownLatch(33);
        if (storage.size() == 0) {
            for (char i = 'А'; i < 'а'; i++) {
                new CalculatingCountLettersInText(i, text, countDownLatch).start();
            }
            new CalculatingCountLettersInText('Ё', text, countDownLatch).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            System.out.println("Error " + e.getMessage());
        }
        return storage;
    }

    /**
     * Метод создает список всех букв, отсортированных  в порядке уменьшения частоты символа.
     *
     * @return объект ArrayList содержащий отсортированный список букв.
     */
    public List<Character> getListLettersSortedByCount() {
        if (storage.size() == 0) {
            getCountOfEachLetter();
        }
        return storage.entrySet()
                .stream()
                .sorted((c1, c2) -> c2.getValue() - c1.getValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private String getFileString() throws IOException {
        final Scanner scanner = new Scanner(Paths.get(fileName), "cp1251");
        return scanner.useDelimiter("\\A").next();
    }

    /**
     * Класс реализующий поток, который подсчитывает количество вхождений символа в строку.
     */
    private class CalculatingCountLettersInText extends Thread {
        private final char letter;
        private final String text;
        private final CountDownLatch countDownLatch;

        public CalculatingCountLettersInText(final char letter, final String text, CountDownLatch count) {
            this.letter = letter;
            this.text = text;
            this.countDownLatch = count;
        }

        @Override
        public void run() {
            int count = 0;
            for (int j = 0; j < text.length(); j++) {
                if (text.charAt(j) == letter) {
                    count++;
                }
            }
            storage.put(letter, count);
            countDownLatch.countDown();
        }
    }
}
