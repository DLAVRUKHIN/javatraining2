package com.tsc.lesson3;

import java.util.List;
import java.util.ListIterator;

import static java.util.Objects.nonNull;

/**
 * Класс содержит методы для однопоточной и многопоточной сортировки коллекции алгоритмом quicksort.
 *
 * @author Дмитрий Лаврухин
 */
public class CollectionSorting {

    /**
     * Сортировка коллекции в однопоточном режиме.
     *
     * @param list список для сортировки.
     * @param <T>  класс объектов в списке.
     */
    public static <T> void singleThreadedSortingMode(List<T> list) {
        if (nonNull(list) && list.size() > 1) {
            Object[] array = list.toArray();
            singleSortingMode(array, 0, array.length - 1);
            fillCollectionFromArray(list, array);
        }
    }

    /**
     * Сортировка коллекции в многопоточном режиме.
     *
     * @param list список для сортировки.
     * @param <T>  класс объектов в списке.
     */
    public static <T> void multiThreadedSortingMode(List<T> list) {
        if (nonNull(list) && list.size() > 1) {
            Object[] array = list.toArray();

            ThreadForSorting sortThread = new ThreadForSorting(array);
            sortThread.start();
            try {
                sortThread.join();
            } catch (InterruptedException e) {
                System.out.println("Error " + e.getMessage());
            }

            fillCollectionFromArray(list, array);
        }
    }

    private static void singleSortingMode(Object[] array, int low, int high) {
        if (low >= high) {
            return;
        }

        int middle = low + (high - low) / 2;
        Object middleElement = array[middle];
        int leftIndex = low;
        int rightIndex = high;

        while (leftIndex <= rightIndex) {
            while (compare(array[leftIndex], middleElement) < 0) {
                leftIndex++;
            }
            while (compare(array[rightIndex], middleElement) > 0) {
                rightIndex--;
            }
            if (leftIndex <= rightIndex) {
                swapElement(array, leftIndex, rightIndex);
                leftIndex++;
                rightIndex--;
            }
        }

        if (low < rightIndex) {
            singleSortingMode(array, low, rightIndex);
        }
        if (high > leftIndex) {
            singleSortingMode(array, leftIndex, high);
        }
    }

    private static void swapElement(Object[] array, int firstIndex, int secondIndex) {
        Object tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static int compare(Object o1, Object o2) {
        return ((Comparable) o1).compareTo(o2);
    }

    @SuppressWarnings({"unchecked"})
    private static <T> void fillCollectionFromArray(List<T> list, Object[] array) {
        ListIterator<T> iterator = list.listIterator();
        for (Object obj : array) {
            iterator.next();
            iterator.set((T) obj);
        }
    }

    /**
     * Класс реализующий поток для сортировки массива алгоритмом quicksort.
     * При работе рекурсивно создаются потоки для сортировки левого и правого подмассивов.
     */
    private static class ThreadForSorting extends Thread {
        private final Object[] array;
        private final int lowIndex;
        private final int highIndex;

        public ThreadForSorting(Object[] array) {
            this.lowIndex = 0;
            this.highIndex = array.length - 1;
            this.array = array;
        }

        public ThreadForSorting(final Object[] array, final int lowIndex, final int highIndex) {
            this.array = array;
            this.lowIndex = lowIndex;
            this.highIndex = highIndex;
        }

        @Override
        public void run() {
            if (lowIndex >= highIndex) {
                return;
            }

            int middle = lowIndex + (highIndex - lowIndex) / 2;
            Object middleElement = array[middle];
            int leftIndex = lowIndex;
            int rightIndex = highIndex;

            while (leftIndex <= rightIndex) {
                while (compare(array[leftIndex], middleElement) < 0) {
                    leftIndex++;
                }
                while (compare(array[rightIndex], middleElement) > 0) {
                    rightIndex--;
                }
                if (leftIndex <= rightIndex) {
                    swapElement(array, leftIndex, rightIndex);
                    leftIndex++;
                    rightIndex--;
                }
            }

            ThreadForSorting rightThread = new ThreadForSorting(array, lowIndex, rightIndex);
            ThreadForSorting leftThread = new ThreadForSorting(array, leftIndex, highIndex);
            if (lowIndex < rightIndex) {
                rightThread.start();
            }
            if (highIndex > leftIndex) {
                leftThread.start();
            }
            try {
                rightThread.join();
                leftThread.join();
            } catch (InterruptedException e) {
                System.out.println("Error " + e.getMessage());
            }
        }
    }
}
