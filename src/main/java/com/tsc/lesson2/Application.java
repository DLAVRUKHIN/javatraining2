package com.tsc.lesson2;

import com.tsc.lesson2.domain.entities.OrganizationStorage;
import com.tsc.lesson2.util.OrganizationStorageReadFromTextFile;
import com.tsc.lesson2.util.OrganizationStorageSerialization;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Application {

    private final static String binaryFileName = "src/main/java/com/tsc/lesson2/organization.dat";

    public static void main(String[] args) {
        OrganizationStorage newOrganizationStorage;

        if (args.length > 0) {
            String textFileName = args[0];
            newOrganizationStorage = readOrganizationStorageFromTxtFile(textFileName);
        } else {
            newOrganizationStorage = createOrganizationStorageFromConsoleInput();
        }

        if (Files.exists(Paths.get(binaryFileName))) {
            OrganizationStorage organizationStorageFromBinaryFile = readOrganizationStorageFromBinaryFile(binaryFileName);
            mergeStorages(newOrganizationStorage, organizationStorageFromBinaryFile);
        }

        OrganizationStorageSerialization.serialization(newOrganizationStorage, binaryFileName);
    }

    /**
     * Метод создает список организаций из текстового файла
     *
     * @param fileName имя файла
     * @return список организаций
     */
    private static OrganizationStorage readOrganizationStorageFromTxtFile(String fileName) {
        return OrganizationStorageReadFromTextFile.read(fileName);
    }

    /**
     * Метод десериализует список организаций из файла
     *
     * @param fileName имя файла
     * @return список организаций
     */
    private static OrganizationStorage readOrganizationStorageFromBinaryFile(String fileName) {
        return OrganizationStorageSerialization.deserialization(fileName);
    }

    /**
     * Метод создает список организаций из строк, введенных в консоль.
     * Ввод оканчивается пустой строкой.
     *
     * @return список организаций
     */
    private static OrganizationStorage createOrganizationStorageFromConsoleInput() {
        OrganizationStorage storage = new OrganizationStorage();
        Scanner reader = new Scanner(System.in);
        String line;
        while (!(line = reader.nextLine()).equals("")) {
            storage.add(line);
        }
        return storage;
    }

    private static void mergeStorages(OrganizationStorage newStorage, OrganizationStorage oldStorage) {
        newStorage.add(oldStorage);
    }
}
