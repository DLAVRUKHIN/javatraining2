package com.tsc.lesson2.util;

import com.tsc.lesson2.domain.entities.OrganizationStorage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Класс предназначен для сериализация и десериализация экземпляра класса OrganizationStorage,
 * содержащего список организаций.
 *
 * @author Дмитрий Лаврухин
 */
public class OrganizationStorageSerialization {

    /**
     * Сериализация объекта OrganizationStorage.
     * @param storage список организаций для сериализации
     * @param fileName имя файла, в который будет сохранен сериализованный объект .
     */
    public static void serialization(OrganizationStorage storage, String fileName) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(storage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Десериализация объекта OrganizationStorage из файла.
     * @param fileName имя файла, содержащего сериализованный объект
     * @return экземпляр класса OrganizationStorage
     */
    public static OrganizationStorage deserialization(String fileName) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            OrganizationStorage storage = (OrganizationStorage) ois.readObject();
            return storage;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


}
