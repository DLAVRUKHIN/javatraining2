package com.tsc.lesson2.util;

import com.tsc.lesson2.domain.entities.OrganizationStorage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.util.Objects.nonNull;

/**
 * класс для чтения списка организаций из текстового файла.
 *
 * @author Дмитрий Лаврухин
 */
public class OrganizationStorageReadFromTextFile {

    /**
     * Создание списка организаций из текстового файла
     * @param fileName имя файла
     * @return экземпляр класса OrganizationStorage
     */
    public static OrganizationStorage read(String fileName) {
        OrganizationStorage storage = new OrganizationStorage();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while (nonNull(line = reader.readLine())) {
                storage.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return storage;
    }
}
