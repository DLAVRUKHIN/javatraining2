package com.tsc.lesson2.domain.enums;

import java.io.Serializable;

/**
 * Перечесление возможных типов юр.лиц.
 *
 * @author Дмитрий Лаврухин
 */
public enum OrganizationType implements Serializable {
    OOO, OAO, ZAO, IP;
}
