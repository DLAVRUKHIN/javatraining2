package com.tsc.lesson2.domain.entities;

import java.io.Serial;
import java.io.Serializable;

/**
 * Класс предназначен для хранения информации о человеке.
 *
 * @author Дмитрий Лаврухин
 */

public class People implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String name;
    private String patronymic;
    private String surname;

    public People(final String surname, final String name, final String patronymic) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Фамилия: " + this.surname +
                " Имя: " + this.name +
                " Отчество: " + this.patronymic;
    }
}
