package com.tsc.lesson2.domain.entities;

import com.tsc.lesson2.domain.enums.OrganizationType;
import com.tsc.lesson2.domain.exception.AuthorizedCapitalException;
import com.tsc.lesson2.domain.exception.LegalEntityTypeException;
import com.tsc.lesson2.domain.exception.ShareholderInformationException;
import com.tsc.lesson2.domain.exception.SharesCompanyException;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Класс предназначенный для хранения информации об юр.лице.
 *
 * @author Дмитрий Лаврухин
 */
public class Organization implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String name;
    private OrganizationType type;
    private People generalDirector;
    private String parentOrganization;
    private final ShareholderStorage shareholderStorage;
    private double authorizedCapital;
    private int sharesCount;

    {
        sharesCount = 100;
    }

    public Organization(final String organizationName,
                        final String organizationType,
                        final String directorName,
                        final String directorSureName,
                        final String directorPatronymic,
                        final String authorizedCapital,
                        final String parentOrganization,
                        final String[] shareholders) {

        this.name = organizationName;
        this.type = createOrganizationType(organizationType);
        this.generalDirector = new People(directorSureName, directorName, directorPatronymic);
        this.authorizedCapital = createAuthorizedCapital(authorizedCapital);
        this.parentOrganization = parentOrganization.equals("NONE") ? null : parentOrganization;
        this.shareholderStorage = new ShareholderStorage(shareholders);
    }

    /**
     * проверка типа юр лица
     *
     * @param type строковое представление типа юр лица
     * @return перечисляемое представление типа юр лица
     * @throws LegalEntityTypeException если введен некорректный тип юр лица
     */
    private OrganizationType createOrganizationType(final String type) {
        try {
            return OrganizationType.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new LegalEntityTypeException(type + " - некоректные тип юридического лица");
        }
    }

    /**
     * проверка уставного капитала
     *
     * @param authorizedCapital строковое представление уставного капитала
     * @return числовое значение уставного капитала
     * @throws AuthorizedCapitalException если введено отрицательное число, либо не число вовсе.
     */
    private double createAuthorizedCapital(final String authorizedCapital) {
        try {
            final double capital = Double.parseDouble(authorizedCapital);
            if (capital <= 0) {
                throw new AuthorizedCapitalException("Значение уставного капитала не может быть меньше нуля. Было введено " + authorizedCapital);
            }
            return capital;
        } catch (NumberFormatException e) {
            throw new AuthorizedCapitalException("Значение уставного капитала должно быть вещественным числом. Было введенно " + authorizedCapital);
        }
    }

    public boolean hasShareholder() {
        return shareholderStorage.checkStorage();
    }

    @Override
    public String toString() {
        return "наименование организации: " + type + " " + name +
                " генеральный директор: " + generalDirector +
                " уставной капитал: " + authorizedCapital +
                " родительская организация: " + parentOrganization +
                " акционеры компании: " + shareholderStorage;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Organization otherOrg = (Organization) obj;
        return otherOrg.name.equals(this.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    public Shareholder getShareholderWithMostShares() {
        return shareholderStorage.getFirst();
    }

    public String getName() {
        return name;
    }

    public String getParentOrganization() {
        return parentOrganization;
    }

    public double getAuthorizedCapital() {
        return authorizedCapital;
    }

    /**
     * Класс для хранения коллекции аукционеров
     *
     * @author Дмитрий Лаврухин
     */
    private class ShareholderStorage implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private final TreeSet<Shareholder> storage;

        {
            storage = new TreeSet<>((Serializable & Comparator<Shareholder>) (e1, e2) -> e2.getPercentageShares() - e1.getPercentageShares());
        }

        public ShareholderStorage(final String[] array) {
            addShareholders(array);
        }

        /**
         * Создание из массива строк списка аукционеров
         *
         * @param array массив строк, содержащий информацию об аукцонерах в формате
         *              {"Фамилия 1","Процент акций 1","Фамилия 2","Процент акций 2"}
         */
        private void addShareholders(final String[] array) {
            for (int i = 0; i < array.length; i += 2) {
                try {
                    final int percentageShares = Integer.parseInt(array[i + 1]);
                    sellShares(percentageShares);
                    storage.add(new Shareholder(array[i], percentageShares));
                } catch (NumberFormatException e) {
                    throw new ShareholderInformationException("Значение пакета аций должно быть вещественным числом. Было введено " + array[i + 1]);
                }
            }
        }

        private void sellShares(final int shares) {
            if (sharesCount - shares < 0) {
                throw new SharesCompanyException("Невозможно выдать " + shares + "% акций кампании. Доступный остаток составляет " + sharesCount + "%.");
            }
            if (shares < 0 || shares > 100) {
                throw new SharesCompanyException("Значение пакета акций должно быть в диапазоне от 0 до 100. Было введено значение " + shares);
            }
            sharesCount -= shares;
        }

        public Shareholder getFirst() {
            return storage.first();
        }

        private boolean checkStorage() {
            return storage.size() > 0;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("акционеры компании: ");
            for (Shareholder s : storage) {
                sb.append(s).append("; ");
            }
            return sb.toString();
        }
    }
}
