package com.tsc.lesson2.domain.entities;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;

import static java.util.Objects.isNull;

/**
 * Класс предназначен для хранения списка организаций,
 * а так же для получения информации о самом богатом акционере
 * и организации, породившей наибольшее количество "дочек".
 *
 * @author Дмитрий Лаврухин
 */

public class OrganizationStorage implements Serializable, Iterable<Organization> {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Set<Organization> storage;

    {
        storage = new HashSet<>();
    }

    public void add(final OrganizationStorage organizationStorage) {
        organizationStorage.forEach(storage::add);
    }

    public void add(final String[] organizationsArray) {
        for (String organizationString : organizationsArray) {
            add(organizationString);
        }
    }

    public void add(final String organizationString) {
        final Organization newOrganization = createOrganization(organizationString);
        storage.add(newOrganization);
    }

    private Organization createOrganization(final String organizationString) {
        final String[] organizationArray = organizationString.split(" ");
        final String organizationName = organizationArray[0];
        final String organizationType = organizationArray[1];
        final String directorSurname = organizationArray[2];
        final String directorName = organizationArray[3];
        final String directorPatronymic = organizationArray[4];
        final String organizationAuthorizedCapital = organizationArray[5];
        final String parentOrganization = organizationArray[6];
        final String[] shareholders = Arrays.copyOfRange(organizationArray, 7, organizationArray.length);
        return new Organization(organizationName, organizationType, directorName, directorSurname, directorPatronymic, organizationAuthorizedCapital, parentOrganization, shareholders);
    }

    /**
     * Поиск самого богатого акционера. Состояние определяется как сумма % акций * уставной капитал фирмы
     *
     * @return фамилия акционера
     */
    public String richestPerson() {
        checkStorageSize();

        double largestCapital = 0;
        String richestSurname = null;

        for (Organization org : storage) {
            if (org.hasShareholder()) {
                final double authorizedCapital = org.getAuthorizedCapital();
                final Shareholder richestShareholder = org.getShareholderWithMostShares();
                final int percentageShares = richestShareholder.getPercentageShares();
                final double shareholdersCapital = authorizedCapital * percentageShares;
                if (shareholdersCapital > largestCapital) {
                    largestCapital = shareholdersCapital;
                    richestSurname = richestShareholder.getSurname();
                }
            }
        }
        return richestSurname;
    }

    /**
     * Поиск организации, создавшей наибольшее количество дочерних организаций
     *
     * @return название организации
     */
    public String organizationWithMostSubsidiaries() {
        checkStorageSize();
        final Map<String, List<String>> connectedOrganizations = new LinkedHashMap<>();
        fillConnectedOrganization(connectedOrganizations);

        return connectedOrganizations.entrySet()
                .stream()
                .max(Comparator.comparingInt(e -> e.getValue().size()))
                .get()
                .getKey();
    }

    /**
     * Заполнение мапы связанных организаций. Ключем является название родительской организации.
     * Значением является список всех связанных организаций, включая родительскую и "дочек дочек".
     *
     * @param connectedOrganizations мапа для заполнения
     */
    private void fillConnectedOrganization(final Map<String, List<String>> connectedOrganizations) {
        storage.forEach(o -> {
            final String orgName = o.getName();
            final String parName = o.getParentOrganization();
            if (isNull(parName)) {
                final List<String> orgList = new LinkedList<>();
                orgList.add(orgName);
                connectedOrganizations.put(orgName, orgList);
            } else {
                for (List<String> list : connectedOrganizations.values()) {
                    if (list.contains(parName)) {
                        list.add(o.getName());
                    }
                }
            }
        });
    }

    private void checkStorageSize() {
        if (storage.size() == 0) {
            throw new NoSuchElementException("В списке нет ни одной организации!");
        }
    }

    @Override
    public Iterator<Organization> iterator() {
        return storage.iterator();
    }

    @Override
    public void forEach(Consumer<? super Organization> action) {
        storage.forEach(action);
    }

    @Override
    public Spliterator<Organization> spliterator() {
        return storage.spliterator();
    }

    @Override
    public String toString() {
        return storage.toString();
    }
}
