package com.tsc.lesson2.domain.entities;

import java.io.Serial;
import java.io.Serializable;

/**
 * Класс описывающий акционеров. Хранит фамилию и процент владения акциями.
 * При указания процента владения проверяется, что он не выходит за интервал между 0 и 100.
 * Иначе бросается IllegalArgumentException.
 *
 * @author Дмитрий Лаврухин
 */
public class Shareholder implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String surname;
    private int percentageShares;

    public Shareholder(final String lastName, final int percentageShares) {
        checkPercentageShares(percentageShares);
        this.surname = lastName;
        this.percentageShares = percentageShares;
    }

    public int getPercentageShares() {
        return percentageShares;
    }

    private void checkPercentageShares(final int percentageShares) {
        if (percentageShares <= 0 || percentageShares > 100) {
            throw new IllegalArgumentException("Процент владения акций должен быть в интервале от 0 до 100. Передан аргумент " + percentageShares + ".");
        }
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return "Фамилия: " + this.surname + " процент акций: " + this.percentageShares;
    }
}
