package com.tsc.lesson2.domain.exception;

/**
 * Исключение для уставного капитала
 *
 * @author  Дмитрий Лаврухин
 */
public class AuthorizedCapitalException extends RuntimeException {
    public AuthorizedCapitalException() {
    }

    public AuthorizedCapitalException(String message) {
        super(message);
    }

    public AuthorizedCapitalException(String message, Throwable cause) {
        super(message, cause);
    }
}
