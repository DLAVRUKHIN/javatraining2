package com.tsc.lesson2.domain.exception;

/**
 * Исключение при создании аукционера
 *
 * @author Дмитрий Лаврухин
 */
public class ShareholderInformationException extends RuntimeException{

    public ShareholderInformationException() {
    }

    public ShareholderInformationException(String message) {
        super(message);
    }

    public ShareholderInformationException(String message, Throwable cause) {
        super(message, cause);
    }
}
