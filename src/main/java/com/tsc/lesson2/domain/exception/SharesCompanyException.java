package com.tsc.lesson2.domain.exception;

/**
 * Исключение для некорректного значения пакета акций
 *
 * @author Дмитрий Лаврухин
 */
public class SharesCompanyException extends RuntimeException{
    public SharesCompanyException() {
    }

    public SharesCompanyException(String message) {
        super(message);
    }

    public SharesCompanyException(String message, Throwable cause) {
        super(message, cause);
    }
}
