package com.tsc.lesson2.domain.exception;

/**
 * Исключение для типа юр лица
 *
 * @author Дмитрий Лаврухин
 */
public class LegalEntityTypeException extends RuntimeException {

    public LegalEntityTypeException() {
    }

    public LegalEntityTypeException(String message) {
        super(message);
    }

    public LegalEntityTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
