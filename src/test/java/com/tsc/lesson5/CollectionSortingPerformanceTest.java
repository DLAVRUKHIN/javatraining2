package com.tsc.lesson5;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CollectionSortingPerformanceTest {

    private final String logFileName = "src/test/java/com/tsc/lesson5/result/SortingCollectionPerformanceTest.txt";

    private final List<Integer> collectionForSingleThreadSortingMode = new ArrayList<>();
    private final List<Integer> collectionForMultiThreadSortingMode = new ArrayList<>();
    private final List<Integer> collectionForMultiThreadForkJoinSortingMode = new ArrayList<>();

    private final int count = 10_000;
    private final StopWatch timer = new StopWatch();

    @Test
    void compareSortingTime() {
        clearFile();
        fillCollections();
        long singleModeTime = singleThreadSortingModeTime();
        long multiForkJoinModeTime = multiThreadForkJoinSortingModeTime();
        long multiModeTime = multiThreadSortingModeTime();
        String singleModeResultString = createResultString("singlethread", singleModeTime);
        String multiModeResultString = createResultString("multithread", multiModeTime);
        String multiForkJoinModeResultString = createResultString("multithread with fork/join framework", multiForkJoinModeTime);
        writeResultInFile(singleModeResultString);
        writeResultInFile(multiModeResultString);
        writeResultInFile(multiForkJoinModeResultString);
    }

    private long singleThreadSortingModeTime() {
        timer.start();
        CollectionSorting.singleThreadedSortingMode(collectionForSingleThreadSortingMode);
        return timer.getElapsedTime();
    }

    private long multiThreadSortingModeTime() {
        timer.start();
        CollectionSorting.multiThreadedSortingMode(collectionForMultiThreadSortingMode);
        return timer.getElapsedTime();
    }

    private long multiThreadForkJoinSortingModeTime() {
        timer.start();
        CollectionSorting.multiThreadedForkJoinSortingMode(collectionForMultiThreadSortingMode);
        return timer.getElapsedTime();
    }

    private void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    private void fillCollections() {
        for (int i = 0; i < count; i++) {
            int value = getRandomInt();
            collectionForMultiThreadSortingMode.add(value);
            collectionForSingleThreadSortingMode.add(value);
            collectionForMultiThreadForkJoinSortingMode.add(value);
        }
    }

    private int getRandomInt() {
        int min = -100;
        int max = 100;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private String createResultString(String mode, long time) {
        final String result = "sorting mode " +
                mode +
                " time " +
                time +
                "\r\n";
        return result;
    }

    private void writeResultInFile(String result) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            writer.write(result);
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
    }
}
