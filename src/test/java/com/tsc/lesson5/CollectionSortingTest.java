package com.tsc.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CollectionSortingTest {
    private List<Integer> collection;

    @BeforeEach
    void createCollection() {
        collection = new ArrayList<>();
        for (int i = 0; i < 10_000; i++) {
            collection.add(getRandomInteger());
        }
    }

    @Test
    void singleThreadedMode_MustSortCollection() {
        CollectionSorting.singleThreadedSortingMode(collection);
        assertThat(collection).isSorted();
    }

    @Test
    void multithreadedMode_MustSortCollection() {
        CollectionSorting.multiThreadedSortingMode(collection);
        assertThat(collection).isSorted();
    }

    @Test
    void multithreadedForkJoinMode_MustSortCollection() {
        CollectionSorting.multiThreadedForkJoinSortingMode(collection);
        assertThat(collection).isSorted();
    }

    private int getRandomInteger() {
        int min = -100;
        int max = 100;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }
}
