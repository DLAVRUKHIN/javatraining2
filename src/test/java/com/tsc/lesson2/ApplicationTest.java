package com.tsc.lesson2;

import com.tsc.lesson2.domain.entities.OrganizationStorage;
import com.tsc.lesson2.util.OrganizationStorageSerialization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class ApplicationTest {

    private final String[] organizations = {
            "Организация1 OOO Иванов Иван Иванович 100000.12 NONE Акционер1.1 10 Акционер1.2 20 Акционер1.3 30",
            "Организация2 ZAO Директоров Директор Директорович 10000.11 Т-1 Акционер2.1 60 Акционер2.2 40",
            "Организация3 OAO Алексеев Алексей Алексеевич 1000000.44 Т-2 Акционер3.1 60 Акционер3.2 40",
            "Организация4 IP Генеральный Генерал Генералович 20000 Т-2 Акционер4.1 10 Акционер4.2 90",
            "Организация5 OOO Сергеев Сергей Сергеевич 12000 Т-1 Акционер5.1 60 Акционер5.2 40"
    };

    private final String[] organizationsToMerge = {
            "Организация1 OOO Иванов Иван Иванович 20000 NONE Акционер1.1 10 Акционер1.2 20",
            "Организация2 ZAO Упский Иван Иванович 10000.11 Т-1 Акционер2.1 60 Акционер2.2 10",
            "Организация4 IP Кук Иван Иванович 2000 Берг"
    };

    private final String[] organizationsAfterMerge = {
            "Организация1 OOO Иванов Иван Иванович 20000 NONE Акционер1.1 10 Акционер1.2 20",
            "Организация2 ZAO Упский Иван Иванович 10000.11 Т-1 Акционер2.1 60 Акционер2.2 10",
            "Организация3 OAO Алексеев Алексей Алексеевич 1000000.44 Т-2 Акционер3.1 60 Акционер3.2 40",
            "Организация4 IP Кук Иван Иванович 2000 Берг",
            "Организация5 OOO Сергеев Сергей Сергеевич 12000 Т-1 Акционер5.1 60 Акционер5.2 40"
    };

    private final String txtFileName = "src/main/java/com/tsc/lesson2/organization.txt";
    private final String datFileName = "src/main/java/com/tsc/lesson2/organization.dat";

    @BeforeEach
    void setUp() {
        final File txtFile = new File(txtFileName);
        final File datFile = new File(datFileName);
        txtFile.delete();
        datFile.delete();
    }

    /**
     * Запуск с параметром имени файла. Файл organizations.dat не существует.
     */
    @Test
    public void main_createDatFileFromTxtFile() {
        createOrganizationsTxtFile(organizations);

        final OrganizationStorage exceptStorage = new OrganizationStorage();

        exceptStorage.add(organizations);
        Application.main(new String[]{txtFileName});

        final boolean existFile = Files.exists(Path.of(datFileName));
        assertThat(existFile).isTrue();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(datFileName))) {
            final OrganizationStorage storage = (OrganizationStorage) ois.readObject();
            assertThat(storage.toString()).isEqualTo(exceptStorage.toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запуск с параметром имени файла. Существует файл organizations.dat .
     */
    @Test
    public void main_createBinaryFileFromTxtAndBinaryFile() {
        createOrganizationsTxtFile(organizationsToMerge);
        createOrganizationDatFile(organizations);

        final OrganizationStorage storageToMerge = new OrganizationStorage();
        storageToMerge.add(organizationsToMerge);
        final OrganizationStorage exceptStorage = new OrganizationStorage();
        exceptStorage.add(organizationsAfterMerge);

        Application.main(new String[]{txtFileName});

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(datFileName))) {
            final OrganizationStorage storage = (OrganizationStorage) ois.readObject();
            assertThat(storage.toString()).isEqualTo(exceptStorage.toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createOrganizationsTxtFile(final String[] organizationsArray) {
        try (PrintStream writer = new PrintStream(txtFileName)) {
            for (String s : organizationsArray) {
                writer.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createOrganizationDatFile(final String[] organizationsArray) {
        final OrganizationStorage storage = new OrganizationStorage();
        storage.add(organizationsArray);
        OrganizationStorageSerialization.serialization(storage, datFileName);
    }
}
