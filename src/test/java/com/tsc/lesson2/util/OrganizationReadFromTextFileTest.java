package com.tsc.lesson2.util;

import com.tsc.lesson2.domain.entities.OrganizationStorage;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

class OrganizationReadFromTextFileTest {

    private final String[] organizations = {
            "Организация1 OOO Иванов Иван Иванович 100000.12 NONE Акционер1.1 10 Акционер1.2 20 Акционер1.3 30",
            "Организация2 OOO Директоров Директор Директорович 10000.11 Т-1 Акционер2.1 60 Акционер2.2 40",
            "Организация3 OOO Алексеев Алексей Алексеевич 1000000.44 Т-2 Акционер3.1 60 Акционер3.2 40",
            "Организация4 OOO Генеральный Генерал Генералович 20000 Т-2",
            "Организация5 OOO Сергеев Сергей Сергеевич 12000 Т-1 Акционер5.1 60 Акционер5.2 40"
    };
    private final String fileName = "src/test/java/com/tsc/lesson2/organization.dat";

    @Test
    void read_MustReturnOrganizationStorage() {
        createOrganizationsTxtFile(organizations);
        final OrganizationStorage exceptStorage = new OrganizationStorage();
        exceptStorage.add(organizations);
        final OrganizationStorage actualStorage = OrganizationStorageReadFromTextFile.read(fileName);
        assertThat(actualStorage.toString()).isEqualTo(exceptStorage.toString());
    }

    private void createOrganizationsTxtFile(final String[] organizationsArray) {
        try (PrintStream writer = new PrintStream(fileName)) {
            for (String s : organizationsArray) {
                writer.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
