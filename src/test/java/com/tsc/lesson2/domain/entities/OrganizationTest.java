package com.tsc.lesson2.domain.entities;

import com.tsc.lesson2.domain.exception.AuthorizedCapitalException;
import com.tsc.lesson2.domain.exception.LegalEntityTypeException;
import com.tsc.lesson2.domain.exception.ShareholderInformationException;
import com.tsc.lesson2.domain.exception.SharesCompanyException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

class OrganizationTest {

    private final String orgName = "Организация1";
    private final String orgType = "OOO";
    private final String directorSureName = "Иванов";
    private final String directorName = "Иван";
    private final String directorPatronymic = "Иванович";
    private final String authorizedCapital = "200000";
    private final String parentOrganization = "NONE";
    private final String[] shareholder = {"Акционер1", "10", "Акционер2", "20", "Акционер3", "30"};

    @Test
    public void constructor_MustReturnAuthorizedCapitalException_WhenCapitalIsNegative() {
        final String capital = "-10000";
        final Throwable result = catchThrowable(() -> new Organization(orgName, orgType, directorName, directorSureName, directorPatronymic, capital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(AuthorizedCapitalException.class)
                .hasMessage("Значение уставного капитала не может быть меньше нуля. Было введено " + capital);
    }

    @Test
    public void constructor_MustReturnAuthorizedCapitalException_WhenNotNumberEntered() {
        final String capital = "capital";
        final Throwable result = catchThrowable(() -> new Organization(orgName, orgType, directorName, directorSureName, directorPatronymic, capital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(AuthorizedCapitalException.class)
                .hasMessage("Значение уставного капитала должно быть вещественным числом. Было введенно " + capital);
    }

    @Test
    public void constructor_MustReturnLegalEntityTypeException_WhenIncorrectValueEntered() {
        final String type = "не ООО";
        final Throwable result = catchThrowable(() -> new Organization(orgName, type, directorName, directorSureName, directorPatronymic, authorizedCapital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(LegalEntityTypeException.class)
                .hasMessage(type + " - некоректные тип юридического лица");
    }

    @Test
    public void constructor_MustReturnSharesCompanyException_WhenEnteredShareMoreBalance() {
        final String[] shareholder = {"Акционер1", "10", "Акционер2", "20", "Акционер3", "90"};
        final Throwable result = catchThrowable(() -> new Organization(orgName, orgType, directorName, directorSureName, directorPatronymic, authorizedCapital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(SharesCompanyException.class)
                .hasMessage("Невозможно выдать 90% акций кампании. Доступный остаток составляет 70%.");
    }

    @Test
    public void constructor_MustReturnShareholderInformationException_WhenShareValueNotNumber() {
        final String[] shareholder = {"Акционер1", "Акционер2", "20", "Акционер3", "90"};
        final Throwable result = catchThrowable(() -> new Organization(orgName, orgType, directorName, directorSureName, directorPatronymic, authorizedCapital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(ShareholderInformationException.class)
                .hasMessage("Значение пакета аций должно быть вещественным числом. Было введено Акционер2");
    }

    @Test
    public void constructor_MustReturnSharesCompanyException_WhenShareIsNegative() {
        final String[] shareholder = {"Акционер1", "10", "Акционер2", "-20", "Акционер3", "40"};
        final Throwable result = catchThrowable(() -> new Organization(orgName, orgType, directorName, directorSureName, directorPatronymic, authorizedCapital, parentOrganization, shareholder));
        assertThat(result)
                .isInstanceOf(SharesCompanyException.class)
                .hasMessage("Значение пакета акций должно быть в диапазоне от 0 до 100. Было введено значение -20");
    }
}
