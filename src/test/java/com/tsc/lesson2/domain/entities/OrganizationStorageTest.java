package com.tsc.lesson2.domain.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrganizationStorageTest {

    private final String[] organizations = {
            "Организация1 OOO Иванов Иван Иванович 100000.12 NONE Акционер1.1 10 Акционер1.2 20 Акционер1.3 30",
            "Организация2 OOO Директоров Директор Директорович 10000.11 Т-1 Акционер2.1 60 Акционер2.2 40",
            "Организация3 OOO Алексеев Алексей Алексеевич 1000000.44 Т-2 Акционер3.1 60 Акционер3.2 40",
            "Организация4 OOO Генеральный Генерал Генералович 20000 Т-2",
            "Организация5 OOO Сергеев Сергей Сергеевич 12000 Т-1 Акционер5.1 60 Акционер5.2 40"
    };

    private final String exceptRichestPerson = "Акционер3.1";
    private final String exceptOrganizationWithMoreSubsidiaries = "Организация1";
    private OrganizationStorage organizationStorage;

    @BeforeEach
    public void createStorage() {
        organizationStorage = new OrganizationStorage();
        for (String s : organizations) {
            organizationStorage.add(s);
        }
    }

    @Test
    public void richestPerson_MustReturnRichestPerson() {
        final String richestPerson = organizationStorage.richestPerson();
        assertThat(richestPerson).isEqualTo(exceptRichestPerson);
    }

    @Test
    public void orgWithMoreSubsidiaries_MustReturnOrganizationWithMoreSubsidiaries() {
        final String organizationWithMoreSubsidiaries = organizationStorage.organizationWithMostSubsidiaries();
        assertThat(organizationWithMoreSubsidiaries).isEqualTo(exceptOrganizationWithMoreSubsidiaries);
    }

}
