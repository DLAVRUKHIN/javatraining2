package com.tsc.lesson3;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CollectionSortingPerformanceTest {

    private final String logFileName = "src/test/java/com/tsc/lesson3/result/SortingCollectionPerformanceTest.txt";

    private final List<Integer> collectionForSingleThreadSortingMode = new ArrayList<>();
    private final List<Integer> collectionForMultiThreadSortingMode = new ArrayList<>();

    private final int count = 10_000;
    private final StopWatch timer = new StopWatch();

    @Test
    void compareSortingTime() {
        clearFile();
        fillCollections();
        long singleModeTime = singleThreadSortingModeTime();
        long multiModeTime = multiThreadSortingModeTime();
        String singleModeResultString = createResultString("singlethread", singleModeTime);
        String multiModeResultString = createResultString("multithread", multiModeTime);
        writeResultInFile(singleModeResultString);
        writeResultInFile(multiModeResultString);

    }

    private long singleThreadSortingModeTime() {
        timer.start();
        CollectionSorting.singleThreadedSortingMode(collectionForSingleThreadSortingMode);
        return timer.getElapsedTime();
    }

    private long multiThreadSortingModeTime() {
        timer.start();
        CollectionSorting.multiThreadedSortingMode(collectionForMultiThreadSortingMode);
        return timer.getElapsedTime();
    }

    private void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    private void fillCollections() {
        for (int i = 0; i < count; i++) {
            collectionForMultiThreadSortingMode.add(getRandomInt());
            collectionForSingleThreadSortingMode.add(getRandomInt());
        }
    }

    private int getRandomInt() {
        int min = -100;
        int max = 100;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private String createResultString(String mode, long time) {
        final String result = "sorting mode " +
                mode +
                " time " +
                time +
                "\r\n";
        return result;
    }

    private void writeResultInFile(String result) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            writer.write(result);
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
    }
}
