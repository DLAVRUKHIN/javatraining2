package com.tsc.lesson3;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

class TextTest {

    private static final String textString = "Проявлением  наибольшего  милосердия в  нашем  мире  является,  на  мой\n" +
            "взгляд, неспособность человеческого разума связать воедино все, что этот мир\n" +
            "в  себя включает. Мы живем на тихом островке невежества посреди темного моря\n" +
            "бесконечности, и нам вовсе не следует плавать на  далекие расстояния. Науки,\n" +
            "каждая из которых  тянет в своем направлении,  до сих пор причиняли нам мало\n" +
            "вреда; однако  настанет  день  и  объединение  разрозненных  доселе обрывков\n" +
            "знания откроет перед  нами  такие ужасающие виды реальной  действительности,\n" +
            "что  мы либо  потеряем рассудок от увиденного,  либо постараемся скрыться от\n" +
            "этого губительного просветления в покое и безопасности нового средневековья.\n" +
            "     Теософы  высказали  догадку  о внушающем  благоговейный  страх  величии\n" +
            "космического цикла, в котором весь наш мир и человеческая раса являются лишь\n" +
            "временными обитателями. От их намеков на странные проявления давно минувшего\n" +
            "кровь  застыла  бы  в жилах, не будь  они  выражены  в  терминах,  прикрытых\n" +
            "успокоительным  оптимизмом. Однако не они  дали мне возможность единственный\n" +
            "раз заглянуть в эти запретные  эпохи: меня дрожь  пробирает по коже, когда я\n" +
            "об этом думаю, и охватывает безумие, когда я вижу это во сне. Этот проблеск,\n" +
            "как и все грозные проблески истины, был вызван случайным соединением воедино\n" +
            "разрозненных фрагментов - в данном случае  одной старой газетной заметки и\n" +
            "записок  умершего  профессора. Я надеялось;  что никому  больше  не  удастся\n" +
            "совершить подобное  соединение; во всяком случае, если мне суждена жизнь, то\n" +
            "я никогда сознательно  не присоединю ни одного  звена к этой ужасающей цепи.\n" +
            "Думаю,  что  и профессор тоже  намеревался хранить в тайне то,  что узнал, и\n" +
            "наверняка  уничтожил  бы свои записи,  если бы внезапная смерть не  помешала\n" +
            "ему.";

    private static final String fileName = "src/test/java/com/tsc/text.txt";
    private static HashMap<Character, Integer> expectedMap;
    private static List<Character> expectedList;

    @BeforeAll
    static void createData() {
        try (PrintWriter writer = new PrintWriter(fileName, "cp1251")) {
            writer.print(textString);
        } catch (IOException e) {
            System.out.println("Ошибка при записи в файл");
        }
        expectedMap = new HashMap<>();
        expectedMap.put('А', 103);
        expectedMap.put('Б', 24);
        expectedMap.put('В', 70);
        expectedMap.put('Г', 23);
        expectedMap.put('Д', 43);
        expectedMap.put('Е', 141);
        expectedMap.put('Ё', 0);
        expectedMap.put('Ж', 15);
        expectedMap.put('З', 29);
        expectedMap.put('И', 100);
        expectedMap.put('Й', 13);
        expectedMap.put('К', 42);
        expectedMap.put('Л', 49);
        expectedMap.put('М', 54);
        expectedMap.put('Н', 114);
        expectedMap.put('О', 156);
        expectedMap.put('П', 32);
        expectedMap.put('Р', 61);
        expectedMap.put('С', 80);
        expectedMap.put('Т', 80);
        expectedMap.put('У', 28);
        expectedMap.put('Ф', 4);
        expectedMap.put('Х', 13);
        expectedMap.put('Ц', 2);
        expectedMap.put('Ч', 18);
        expectedMap.put('Ш', 10);
        expectedMap.put('Щ', 3);
        expectedMap.put('Ъ', 1);
        expectedMap.put('Ы', 30);
        expectedMap.put('Ь', 25);
        expectedMap.put('Э', 8);
        expectedMap.put('Ю', 8);
        expectedMap.put('Я', 39);

        expectedList = new ArrayList<>();
        expectedList.addAll(Arrays.asList('О', 'Е', 'Н', 'А', 'И', 'С', 'Т', 'В', 'Р', 'М', 'Л', 'Д', 'К', 'Я', 'П', 'Ы', 'З', 'У', 'Ь', 'Б', 'Г', 'Ч', 'Ж', 'Й', 'Х', 'Ш', 'Э', 'Ю', 'Ф', 'Щ', 'Ц', 'Ъ', 'Ё'));
    }

    @Test
    void constructor_mustReturnIOException_whenFileDoesNotExists() {
        final String nonExistentFile = "src/test/java/com/tsc/nonExistentFile.txt";
        Throwable result = catchThrowable(() -> new Text(nonExistentFile));
        assertThat(result).isInstanceOf(IOException.class);

    }

    @Test
    void getListLettersSortedByCount_mustReturnListLettersSortedByCount() {
        try {
            Text text = new Text(fileName);
            List<Character> result = text.getListLettersSortedByCount();
            assertThat(result).isEqualTo(expectedList);
        } catch (IOException e) {
            System.out.println("Ошибка при чтении файла");
        }
    }

    @Test
    void getCountOfEachLetter_mustReturnMapCharacterCount() {
        try {
            Text text = new Text(fileName);
            Map<Character, Integer> result = text.getCountOfEachLetter();
            assertThat(result).isEqualTo(expectedMap);
        } catch (IOException e) {
            System.out.println("Ошибка при чтении файла");
        }
    }

}
